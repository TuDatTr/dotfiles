# clean-nushell-db

#!/usr/bin/env nu
let db = "~/dotfiles/nushell/.config/nushell/history.sqlite3"

def get_current_row [] {
  let current_row = (^sqlite3 $db "SELECT COUNT(*) FROM history h")
  echo $"current rows: ($current_row)"
}

get_current_row
# Remove failed commands
sqlite3 $db "DELETE FROM history WHERE exit_status != 0"
# Remove duplicates. But keep one.
# https://stackoverflow.com/a/53693544/6000005
sqlite3 $db "DELETE FROM history WHERE id NOT IN (SELECT MIN(id) FROM history h  GROUP BY command_line);"
get_current_row 
